#!/usr/bin/env bash
err=`tput setaf 1`
clear=`tput sgr 0`
disks=$(sudo parted -l | grep /dev/); # gets all available disks
time=$1
background=$2

echo $date
if [ -z "$(command -v dnf)" ]; then
    sudo yum -y install smartmontools
else
    sudo dnf -y install smartmontools
fi

for i in ${disks[*]}
do
  if [[ $i == *"/dev/"* ]]; then
      disk=${i%?};
      echo $err $disk;

      ssupport=$(sudo smartctl -i $disk | grep "SMART support is:")

      if [[ $ssupport == *"Available"* ]]; then
          sudo smartctl -c $disk | grep -A1 "${time^} self-test routine"
          echo $clear
          if [[ $background == "true" ]]; then
              sudo smartctl -t $time $disk
          else
              finish=$(sudo smartctl -t $time $disk | grep "Test will complete after")
              finish=${finish#"Test will complete after "};
              finish=$(date -d $finish +%s)
              echo $finish

              now=$(date +%s)
              sec=$(expr $finish-$now)
              echo $sec

          fi
      else
          echo "$err SMART is not supported on disk $disk $clear"
      fi
  fi
done
