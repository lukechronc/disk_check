#!/usr/bin/env bash
set -e
if [ "$EUID" -ne 0 ]
  then echo "This script needs to be run as root"
  exit 1
fi

disks=$( parted -l | grep /dev/); # gets all available disks
time=$1
printlog=$2
background=$3
log_start="Disk self-check ran on $(date)\n\n"
log="log.txt"

if [[ $time == "" ]]; then
  echo Error: must provide lenght argument e.g. short, long
  exit 1
fi

if [ -z "$(command -v dnf)" ]; then
     yum -y install smartmontools
else
     dnf -y install smartmontools
fi

for i in ${disks[*]}
do
  if [[ $i == *"/dev/"* ]]; then
      disk=${i%?};

      ssupport=$( smartctl -i $disk | grep "SMART support is:")
      if [[ $ssupport == *"Available"* ]]; then

          printf "\nStarting test on disk: $disk \n";
           smartctl -c $disk | grep -A1 "${time^} self-test routine"

          if [[ $background == "true" ]]; then
               smartctl -t $time $disk
          else
            smartctl -t $time $disk > /dev/null
            status=$( smartctl -a $disk | grep "Self-test execution status")
            while [[ $status == *"Self-test routine in progress"* ]]; do
              sleep 5
              status=$( smartctl -a $disk | grep "Self-test execution status")
            done

            report=$( smartctl -a $disk | sed -n '/DATA SECTION === :/,/SMART Error Log Version/p')
            echo $report
            if [[ "$report" -ne *"SMART overall-health self-assessment test result: FAILED"* ]]; then
              exit 5
            fi

            echo -e $log_start > $log
            echo -e "DISK=$disk\n$report\n\n$( smartctl -a $disk | sed -n '/Thresholds:/,/SMART Error Log Version/p')\n" >> $log
            echo -e "For more detailed information use smartctl -a $disk" >> $log
            if [[ $printlog == "true" ]]; then
              cat $log
            fi
          fi
      else
          printf " SMART is not supported on disk $disk"
          exit 2
      fi
  fi
done
exit 0
