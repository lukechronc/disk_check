#!/usr/bin/env bash
set -e
disks=$(sudo parted -l | grep /dev/); # gets all available disks
time=$1
printlog=$2
background=$3
log_start="Disk self-check ran on $(date)\n\n"
log="log.txt"

if [ -z "$(command -v dnf)" ]; then
    sudo yum -y install smartmontools
else
    sudo dnf -y install smartmontools
fi

for i in ${disks[*]}
do
  if [[ $i == *"/dev/"* ]]; then
      disk=${i%?};

      ssupport=$(sudo smartctl -i $disk | grep "SMART support is:")
      if [[ $ssupport == *"Available"* ]]; then

          printf "\nStarting test on disk: $disk \n";
          sudo smartctl -c $disk | grep -A1 "${time^} self-test routine"

          if [[ $background == "true" ]]; then
              sudo smartctl -t $time $disk

          else
            sudo smartctl -t $time $disk > /dev/null
            status=$(sudo smartctl -a $disk | grep "Self-test execution status")
            while [[ $status == *"Self-test routine in progress"* ]]; do
              sleep 5
              status=$(sudo smartctl -a $disk | grep "Self-test execution status")

            done
            report=$(sudo smartctl -a $disk | grep -A1 "=== START OF READ SMART DATA SECTION ===")


            echo -e $log_start > $log
            echo -e "DISK=$disk\n$report\n\n$(sudo smartctl -a /dev/sda | sed -n '/Thresholds:/,/SMART Error Log Version/p')\n" >> $log
            echo -e "For more detailed information use smartctl -a $disk" >> $log
            if [[ $printlog == "true" ]]; then
              cat $log
            fi
          fi
      else
          printf " SMART is not supported on disk $disk"

      fi
  fi
done
